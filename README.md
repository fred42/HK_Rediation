## HK radiation E-mail notification 

Based on data from the Hong Kong Observatory [https://www.hko.gov.hk/en/index.html].

## Requirements
- Python 3.6+
- requests
- mailtrap
- python-dotenv

## Usage
1. install the requirements
```bash 
pip install -r requirements.txt
```
2. create the .env file
```bash
# for sending email
MAILTRAP_API_KEY=your_mailtrap_api_key
MAIL_FROM=mailtrap@you_domain_within_mailtrap
MAIL_TO=the_mail_you_want_to_send_to
THRESHOLD=0.3

# data source
RADIATION_URL=https://www.hko.gov.hk/radiation/monitoring/data/rmn_hourly_mean_used.txt
```
3. Run
```bash
python radiation.py
```

4. cron job
```bash
# check every half an hour
15,45 * * * * python /path/to/radiation.py
```

## Notes
- This project uses mailtrap [https://mailtrap.io/] to send emails, you need to bind your own domain name to send emails. You can send 1000 emails per month for free. You don't need a cell phone number or a payment method to sign up for an account.
- This project uses requests to obtain hourly radiation detection data from the Hong Kong Observatory.
- The data log file is in the project root directory and is named all_data_log.log. If the radiation level exceeds 0.3, an email alert will only be sent once every 6 hours.