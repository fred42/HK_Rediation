import mailtrap as mt
import requests
from datetime import datetime
from dotenv import dotenv_values
import os


def check_string_in_file(file_path, target_string):
    try:
        with open(file_path, "r") as file:
            file_content = file.read()
            if target_string in file_content:
                return True
            else:
                return False
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return False


# 脚本所在目录
script_dir = os.path.dirname(os.path.abspath(__file__))
# 文件路径
last_email_time_file = os.path.join(script_dir, "last_email_time.txt")
all_data_log_file = os.path.join(script_dir, "all_data_log.txt")

# 加载环境变量
config = dotenv_values(os.path.join(script_dir, ".env"))

# 获取 MAILTRAP_API_KEY
mailtrap_api_key = config["MAILTRAP_API_KEY"]
mail_from = config["MAIL_FROM"]
mail_to = config["MAIL_TO"]
threshold = float(config["THRESHOLD"])
url = config["RADIATION_URL"]

warning = False

# 获取当前时间戳
current_timestamp = int(datetime.timestamp(datetime.now()))

# 读取上次发送邮件的时间戳
try:
    with open(last_email_time_file, "r") as f:
        last_email_timestamp = int(f.read())
except FileNotFoundError:
    last_email_timestamp = 0

# 构建API URL
api_url = f"{url}?_={current_timestamp}"

# API请求头
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0",
    "Accept": "text/plain, */*; q=0.01",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "Referer": "https://www.hko.gov.hk/tc/radiation/monitoring/index.html",
    "X-Requested-With": "XMLHttpRequest",
    "DNT": "1",
    "Connection": "keep-alive",
    "Cookie": f"Path=/; Path=/; last_refreshed_time={current_timestamp-9352}; displayGensit=true; fontSize=0; lang=1",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "TE": "trailers",
}

try:
    # 请求API获取数据
    response = requests.get(api_url, headers=headers)
except Exception as e:
    print(f"请求API失败：{e}")
    with open(all_data_log_file, "a") as f:
        f.write(f'NET ERROR AT {datetime.now()}:\n')
    exit()

data_lines = response.text.split("\n")

# 获取所有数据行的数据
all_data = [line.split() for line in data_lines if line.strip()]

if check_string_in_file(all_data_log_file, data_lines[0]):
    print(f"数据尚未更新：{data_lines[0]}")
    exit()

# 提取所有数值（排除第一行）
all_values = [float(value) for line in all_data[1:] for value in line[1:]]

# 判断是否有数值大于threshold
if any(value > threshold and value < 9999 for value in all_values):
    warning = True
if warning:
    if current_timestamp - last_email_timestamp >= 6 * 3600:
        # 构建邮件内容
        subject = "Warning Raditation greater than 0.3"
        message = response.text

        # 创建邮件对象
        mail = mt.Mail(
            sender=mt.Address(email=mail_from, name="Mailtrap Notification"),
            to=[mt.Address(email=mail_to)],
            subject=subject,
            text=message,
            category="radiation",
        )

        # 发送邮件
        client = mt.MailtrapClient(token=mailtrap_api_key)
        client.send(mail)

        print("邮件已发送")

        # 更新最后发送邮件的时间
        with open(last_email_time_file, "w") as f:
            f.write(str(current_timestamp))
    else:
        print("邮件已发送，但未到冷卻时间")
else:
    print("")

# 将所有数据写入日志文件
with open(all_data_log_file, "a") as f:
    if warning:
        log_header = f"WARNING AT {datetime.now()}:\n"
    else:
        log_header = f"PEACE AT {datetime.now()}:\n"
    print(log_header)
    f.write(log_header + response.text)
